FROM ubuntu:20.04

WORKDIR /work

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt-get install -y vim git cmake make build-essential \
    python3 python3-dev python3-pip && \ 
    apt clean && \
    pip3 install gym

RUN echo 'export PYTHONPATH=/work/gym_cpp/include/gym_wrapper:$PYTHONPATH' >> ~/.bashrc

RUN git clone https://gitlab.com/TING2938/gym_cpp.git && \
    cd gym_cpp && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_BUILD_TYPE=Release && \
    make  

###############################################################################################
CMD ["bash", "-c", "/work/gym_cpp/build/train_basic_test"]





