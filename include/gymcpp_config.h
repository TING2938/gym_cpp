#ifndef __GYMCPP_CONDIG_H__
#define __GYMCPP_CONDIG_H__

#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

namespace gymcpp
{
    using Int = int32_t;
    using Float = float;

    using std::string;
    using Veci = std::vector<Int>;
    using Vecd = std::vector<double>;
    using Vecf = std::vector<Float>;
}

#endif // !__GYMCPP_CONDIG_H__