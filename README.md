# gym_cpp

> author: yeting

> email : yeting2938@163.com

---

## C++ high-performance gym environment framework

### Dependence

```bash
apt install python3 python3-dev cmake
pip3 install gym
```

### Usage

1. clone code

```bash
git clone https://gitlab.com/TING2938/gym_cpp.git
```

2. set python path

```bash
export PYTHONPATH=/path/to/gym_cpp/include/gym_wrapper:$PYTHONPATH
```

3. build it and run

```bash
cd gym_cpp
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make 

# run it
./train_basic_test
./train_qlearning_cliffwalking
```

### run with docker

You can also use docker to build it:

```bash
git clone https://gitlab.com/TING2938/gym_cpp.git
cd gym_cpp
docker build -t gymcpp .
docker run --rm gymcpp
```