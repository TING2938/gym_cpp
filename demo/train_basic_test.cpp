#include <chrono>
#include <random>
#include <iostream>
#include "gymcpp.h"

using namespace std::chrono;
using namespace gymcpp;

int main()
{
    Gym_cpp env;
    // CliffWalking-v0
    // MountainCar-v0
    // CartPole-v0
    env.make("CartPole-v1");
    auto action_space = env.action_space();
    auto obs_space = env.obs_space();
    assert(action_space.bDiscrete);
    assert(!obs_space.bDiscrete);

    auto obs = obs_space.getEmptyObs();
    auto next_obs = obs_space.getEmptyObs();
    Action action = action_space.getEmptyAction();
    Float reward;
    bool done;

    std::default_random_engine engine;
    std::uniform_int_distribution<int> dist(0, action_space.n - 1);

    int total_episode = 2000;
    size_t count = 0;

    auto begin_time = steady_clock::now();

    for (int episode = 0; episode < total_episode; episode++)
    {
        env.reset(&obs);
        for (int t = 0; t < env.max_episode_steps; t++)
        {
            action.front() = dist(engine);
            env.step(action, &next_obs, &reward, &done);
            obs = next_obs;
            count++;
            if (done) break;
        }
    }
    env.close();

    auto end_time = steady_clock::now();

    std::cout << "spend time: " << duration_cast<milliseconds>(end_time - begin_time).count() << " ms" << std::endl;
    std::cout << "step count: " << count << "\n"
              << "total episode: " << total_episode << std::endl;
}
