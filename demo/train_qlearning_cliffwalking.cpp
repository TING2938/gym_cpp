#include "gymcpp.h"
#include <tuple>
#include <random>

using namespace gymcpp;

// observation space: discrete
// action space: discrete
class Qlearning_agent
{
public:
    void init(Int obs_n, Int act_n, Float learning_rate = 0.01, Float gamma = 0.9, Float e_greed = 0.1)
    {
        this->act_n = act_n;
        this->obs_n = obs_n;
        this->learning_rate = learning_rate;
        this->gamma = gamma;
        this->e_greed = e_greed;
        this->Q.resize(obs_n, Vecf(act_n, 0.0));
    }

    // 根据观测值，采样输出动作，带探索过程
    void sample(const State &obs, Action *action)
    {
        std::uniform_real_distribution<Float> u(0.0, 1.0);
        if (u(this->engine) < (1.0 - this->e_greed))
        {
            this->predict(obs, action);
        }
        else
        {
            std::uniform_int_distribution<Int> u_int(0, this->act_n-1);
            action->front() = u_int(this->engine);
        }
    }

    // 根据输入观测值，预测下一步动作
    void predict(const State &obs, Action *action)
    {
        auto &Q_list = this->Q[obs.front()];
        auto maxQ = *std::max_element(Q_list.begin(), Q_list.end());
        Veci action_list;
        for (int i = 0; i < Q_list.size(); i++)
        {
            if (Q_list[i] == maxQ)
                action_list.push_back(i);
        }
        std::uniform_int_distribution<Int> u(0, action_list.size() - 1);
        action->front() = action_list[u(this->engine)];
    }

    void learn(const State &obs, const Action &action, Float reward, const State &next_obs, bool done)
    {
        auto predict_Q = this->Q[obs.front()][action.front()];
        Float target_Q = 0.0;
        if (done)
        {
            target_Q = reward;
        }
        else
        {
            target_Q = reward + this->gamma * (*std::max_element(this->Q[next_obs.front()].begin(), this->Q[next_obs.front()].end()));
        }
        this->Q[obs.front()][action.front()] += this->learning_rate * (target_Q - predict_Q);
    }

private:
    Int act_n;
    Int obs_n;
    Float learning_rate;
    Float gamma;
    Float e_greed;
    std::vector<Vecf> Q;

    std::default_random_engine engine;

}; // !class Qlearning_agent

std::tuple<Float, Int> run_episode(Gym_cpp &env, Qlearning_agent &agent, State &obs, State &next_obs, Action &action, Float &reward, bool &done, bool bRender = false)
{
    Int total_steps = 0;
    Float total_reward = 0.0;
    env.reset(&obs);
    while (true)
    {
        agent.sample(obs, &action); // greedy sample
        env.step(action, &next_obs, &reward, &done);
        agent.learn(obs, action, reward, next_obs, done);

        obs = next_obs;
        total_reward += reward;
        total_steps += 1;
        if (bRender)
        {
            env.render();
        }
        if (done)
        {
            break;
        }
    }
    return {total_reward, total_steps};
}

void test_episode(Gym_cpp &env, Qlearning_agent &agent, State &obs, State &next_obs, Action &action, Float &reward, bool &done)
{
    Float total_reward = 0.0;
    env.reset(&obs);
    while (true)
    {
        agent.predict(obs, &action); // predict according to Q table
        env.step(action, &obs, &reward, &done);
        total_reward += reward;
        sleep(1);
        env.render();
        if (done)
        {
            printf("test reward = %.1f\n", total_reward);
            break;
        }
    }
}

int main()
{
    Float learning_rate = 0.1;
    Float gamma = 0.9;
    Float e_greed = 0.1;

    Gym_cpp env;
    // MountainCar-v0
    // CartPole-v0
    // CliffWalking-v0
    env.make("CliffWalking-v0"); // 0 up, 1 right, 2 down, 3 left
    auto action_space = env.action_space();
    auto obs_space = env.obs_space();
    assert(action_space.bDiscrete);
    assert(obs_space.bDiscrete);
    printf("action space: %d, obs_space: %d\n", action_space.n, obs_space.n);

    Qlearning_agent agent;
    agent.init(obs_space.n, action_space.n, learning_rate, gamma, e_greed);

    auto obs = obs_space.getEmptyObs();
    auto next_obs = obs_space.getEmptyObs();
    auto action = action_space.getEmptyAction();
    Float reward;
    bool done;

    bool bRender = false;
    for (int episode = 0; episode < 500; episode++)
    {
        auto ret = run_episode(env, agent, obs, next_obs, action, reward, done, bRender);
        printf("Episode %d: steps = %d, reward = %.1f\n", episode, std::get<1>(ret), std::get<0>(ret));

        if (episode % 20 == 0)
        {
            bRender = true;
        }
        else
        {
            bRender = false;
        }
    }
    test_episode(env, agent, obs, next_obs, action, reward, done);
    env.close();
}
